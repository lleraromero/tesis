A continuación presentamos los experimentos realizados para determinar cómo varía la precisión de \Corral a medida que el código a analizar se vuelve más complejo. Otro de los objetivos de estos casos de estudio es poder comparar los resultados obtenidos con todas las implementaciones existentes de generación de EPAs. De esta manera, esperamos obtener un primer indicio de la utilidad de utilizar \Corral para generar EPAs.

Para llevar a cabo estas pruebas creamos una clase utilizando código C\# (Figura \ref{fig:clase-base-basicos}) para la cual generamos diversas EPAs. Utilizamos las EPAs como representación del resultado ya que nos permite visualizar de una forma amigable las conclusiones a las que arriba \Corral. Por otro lado, otra de las ventajas que ofrece utilizar las abstracciones es que nos brinda un lenguaje en común para poder comparar los resultados que se pueden obtener con las distintas implementaciones.

Es interesante notar que, aunque nuestros casos de estudio son lo suficientemente simples como para poder validarlos manualmente, esta metodología permite comparar resultados a mayor escala. Esto se debe a que es posible contrastar las EPAs obtenidas y obtener aquellos estados o transiciones que no pertenecen a la intersección de las mismas.

Con respecto a la clase utilizada para los experimentos, esta cuenta con un campo `Estado' inicializado con el valor 0 en el constructor de la clase y dos métodos testigos. El objetivo de incluir dos métodos testigos es poder visualizar en la EPA los posibles estados internos a los que pueden llegar las instancias.

A continuación veremos como, a partir de esta clase base, analizamos la incorporación de diversos métodos que presentan las variadas situaciones con las que nos podemos encontrar al analizar código. En particular, sesgamos los casos de estudios con el fin de buscar los límites de la precisión de \Corral.

Con respecto a la configuración de hardware utilizada para realizar estos experimentos vale la pena mencionar que se utilizó una máquina con 1 core físico Intel Core i7 3.1 GHz junto con 4GB de RAM. En lo referente a la configuración de software se utilizaron todas las implementaciones con sus configuraciones por defecto. La única excepción es el \textit{RecursionBound} de \Corral que fue definido con un valor de 3.

\begin{figure}
    \lstinputlisting[firstline=9, lastline=17]{soundness-experiment/TestCorral.cs}
    \lstinputlisting[firstline=40, lastline=48]{soundness-experiment/TestCorral.cs}
    \lstinputlisting[firstline=81, lastline=81]{soundness-experiment/TestCorral.cs}
    \lstinputlisting[firstline=105, lastline=105]{soundness-experiment/TestCorral.cs}
	\caption{Clase base utilizada para los experimentos}\label{fig:clase-base-basicos}
\end{figure}

\subsubsection{Caso 1: Ciclo más chico que la cota de recursión}
En el primer método que consideramos (Figura \ref{fig:codigo-CicloMasChicoQueRecursionBound}), evaluamos el caso más simple de todos. En este caso \Corral debería ser capaz de recorrer el ciclo en toda su profundidad ya que la cota de recursión supera la cantidad de iteraciones totales del ciclo y por lo tanto poder recorrer todo el código del método.

Al tener una precondición que exige que el `Estado' sea 0, el único resultado posible es que las instancias pasen a tener el `Estado' en 1.

\begin{figure}[H]
    \lstinputlisting[firstline=31, lastline=38]{soundness-experiment/TestCorral.cs}
    \caption{Código del experimento CicloMasChicoQueRecursionBound}\label{fig:codigo-CicloMasChicoQueRecursionBound}
\end{figure}

Las tres implementaciones existentes lograron obtener la EPA esperada como se puede apreciar en las Figuras \ref{fig:corral-CicloMasChicoQueRecursionBound}, \ref{fig:codecontracts-CicloMasChicoQueRecursionBound} y \ref{fig:blast-CicloMasChicoQueRecursionBound}. Sin embargo, en este caso empezamos a detectar la dificultad que demuestra \CodeContracts para poder arribar a conclusiones concretas.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/corral/CicloMasChicoQueRecursionBound.png}
	\caption{Resultado generado con \Corral para CicloMasChicoQueRecursionBound}\label{fig:corral-CicloMasChicoQueRecursionBound}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/codecontracts/CicloMasChicoQueRecursionBound.png}
	\caption{Resultado generado con \CodeContracts para CicloMasChicoQueRecursionBound}\label{fig:codecontracts-CicloMasChicoQueRecursionBound}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/blast/CicloMasChicoQueRecursionBound.png}
	\caption{Resultado generado con \Blast para CicloMasChicoQueRecursionBound}\label{fig:blast-CicloMasChicoQueRecursionBound}
\end{figure}
\clearpage

\subsubsection{Caso 2: Ciclo más largo que la cota de recursión}
Para agregar un poco más de dificultad, intentamos ver que sucede si la cota de recursión no es suficiente para analizar todo el código. Intuitivamente, este caso es similar al anterior. El único estado al que se puede ir, utilizando este método, es al que tiene habilitado únicamente el `TestigoEstado10'.

\begin{figure}[H]
    \lstinputlisting[firstline=21, lastline=29]{soundness-experiment/TestCorral.cs}
    \caption{Código del experimento CicloMasLargoQueRecursionBound}
\end{figure}

La EPA obtenida al utilizar \Corral contiene una gran cantidad de transiciones que no pudieron ser confirmadas (Figura \ref{fig:caso2-corral}). Esto significa que \Corral no fue capaz de determinar qué puede pasar con las instancias una vez ejecutado el método, si este cuenta con un ciclo cuya cota supera el límite de recursión que se está dispuesto a analizar. Sin embargo, lo que podemos aseverar en este caso es que el resultado que ofrece es sound, ya que todas las posibilidades están cubiertas por la abstracción obtenida.

Al igual que en el caso anterior \CodeContracts no fue capaz de confirmar la transición que afirma la posibilidad de pasar al estado en donde la única acción habilitada es `TestigoEstado10' (Figura \ref{fig:caso2-codecontracts}). Es interesante notar que \CodeContracts es capaz de obtener más información acerca del ciclo que la que pudo conseguir \Corral, aunque esta no haya sido lo suficiente como para demostrar la transición.

Por su parte \Blast pudo determinar el resultado correcto del método, quedando como resultado la EPA esperada que se puede ver en la Figura \ref{fig:caso2-blast}.
\clearpage

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/corral/CicloMasLargoQueRecursionBound.png}
	\caption{Resultado generando con \Corral para CicloMasLargoQueRecursionBound}\label{fig:caso2-corral}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/codecontracts/CicloMasLargoQueRecursionBound.png}
	\caption{Resultado generando con \CodeContracts para CicloMasLargoQueRecursionBound}\label{fig:caso2-codecontracts}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/blast/CicloMasLargoQueRecursionBound.png}
	\caption{Resultado generando con \Blast para CicloMasLargoQueRecursionBound}\label{fig:caso2-blast}
\end{figure}

\subsubsection{Caso 3: Ciclo acotado por un parámetro (no determinismo)}
¿Y si se introduce no determinismo en los métodos? ¿Cómo se comporta \Corral con respecto al no determinismo? Con el código de la Figura \ref{caso3-codigo} se prende analizar el caso en el cual la cota del ciclo depende de un parámetro y por consiguiente el resultado del ciclo es no determinístico. 

\begin{figure}[H]
    \lstinputlisting[firstline=50, lastline=58]{soundness-experiment/TestCorral.cs}
    \caption{Código del experimento CicloBasadoEnParam}\label{caso3-codigo}
\end{figure}

Como se puede ver en la Figura \ref{fig:caso3-corral} \Corral es capaz de encontrar valores de parámetros para aquellas transiciones que no exceden el límite de recursión definido. Al igual que en los casos anteriores, al no poder explorar ni inferir el resultado del ciclo cuando la cota es mayor al límite disponible de recursión, es conservador en su resultado.

En el caso de \CodeContracts (Figura \ref{fig:caso3-codecontracts}) la EPA obtenida es la esperada, es decir que hay valores para el parámetro que hacen posible llegar a cualquier valor de `Estado'. Como lo logrado hasta el momento, se puede observar que es capaz de dejar únicamente las transiciones factibles aunque no tiene el poder suficiente como para confirmarlas.

Por su parte, \Blast detecta las mismas transiciones posibles que el resto de las tecnologías (Figura \ref{fig:caso3-blast}). Sin embargo, en este caso no es capaz de demostrar la veracidad de ninguna de ellas de forma fehaciente, al igual que \CodeContracts. Este es el primer caso que encontramos en el cual el resultado de \Corral es más preciso que el de \Blast dando evidencia de la factibilidad de mejora en la precisión de las EPAs generadas.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/corral/CicloBasadoEnParam.png}
	\caption{Resultado generando con \Corral para CicloBasadoEnParam}\label{fig:caso3-corral}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/codecontracts/CicloBasadoEnParam.png}
	\caption{Resultado generando con \CodeContracts para CicloBasadoEnParam}\label{fig:caso3-codecontracts}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=.7]{soundness-experiment/blast/CicloBasadoEnParam.png}
	\caption{Resultado generando con \Blast para CicloBasadoEnParam}\label{fig:caso3-blast}
\end{figure}

\subsubsection{Caso 4: Ciclo con modificaciones espurias}
Como pudimos ver previamente \Corral tiene dificultades para analizar código que está más allá de la cota de recursión. Por este motivo, en este caso nos interesa ver qué pasa si las modificaciones que hace el ciclo son espurias. ¿Sigue siendo sound el resultado?

\begin{figure}[H]
    \lstinputlisting[firstline=60, lastline=69]{soundness-experiment/TestCorral.cs}
    \caption{Código del experimento CicloDeberiaIrA10}\label{caso4-codigo}
\end{figure}

Al igual que en los casos anteriores el resultado sigue siendo sigue siendo una sobre-aproximación de la EPA esperada (Figura \ref{caso4-corral}). En particular, es la EPA correcta aunque no se tenga certeza de una de sus transiciones. Es interesante notar que a pesar de no ser capaz de analizar en profundidad todo el ciclo, \Corral tiene la posibilidad de inferir que las modificaciones que realiza el ciclo, de finalizar (al estar basado en \Boogie no prueba terminación del ciclo), van a ser reescritas por la instrucción inmediatamente posterior. 

A su vez, tanto \CodeContracts (Figura \ref{caso4-codecontracts}) como \Blast (Figura \ref{caso4-blast}) pudieron entender el código a la perfección dando una EPA 100\% precisa.

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/corral/CicloDeberiaIrA10.png}
	\caption{Resultado generando con \Corral para CicloDeberiaIrA10}\label{caso4-corral}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/codecontracts/CicloDeberiaIrA10.png}
	\caption{Resultado generando con \CodeContracts para CicloDeberiaIrA10}\label{caso4-codecontracts}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/blast/CicloDeberiaIrA10.png}
	\caption{Resultado generando con \Blast para CicloDeberiaIrA10}\label{caso4-blast}
\end{figure}

\subsubsection{Caso 5: Ciclo más largo que la cota pero de una sola iteración}
Para hacer el análisis un poco más complejo nos preguntamos qué pasaría si el ciclo terminara en distintas iteraciones. ¿\Corral llega a un resultado incorrecto? En los próximos casos lo ponemos a prueba con diferentes métodos.

En particular, a través del código de la Figura \ref{caso5-codigo} utilizamos un ciclo que siempre termina en la primera iteración.

\begin{figure}[H]
    \lstinputlisting[firstline=71, lastline=80]{soundness-experiment/TestCorral.cs}
    \caption{Código del experimento CicloNoDeberiaIrA10}\label{caso5-codigo}
\end{figure}

En este caso, debido a que el ciclo termina siempre antes de sobrepasar la cota de recursión tanto \Corral (Figura \ref{caso5-corral}) como \Blast (Figura \ref{caso5-blast}) pudieron llegar a la EPA esperada. Sin embargo, \CodeContracts obtuvo el peor resultado conseguido hasta el momento (Figura \ref{caso5-codecontracts}). No sólo no fue capaz de pasar el obstáculo del ciclo que termina prematuramente, sino que incluyó en el resultado estados abstractos que no contienen ninguna instancia. Esto se debe a que no pudo demostrar que los invariantes de dichos estados son insatisfactibles.
\clearpage

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{soundness-experiment/corral/CicloNoDeberiaIrA10.png}
	\caption{Resultado generando con \Corral para CicloNoDeberiaIrA10}\label{caso5-corral}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{soundness-experiment/codecontracts/CicloNoDeberiaIrA10.png}
	\caption{Resultado generando con \CodeContracts para CicloNoDeberiaIrA10}\label{caso5-codecontracts}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics{soundness-experiment/blast/CicloNoDeberiaIrA10.png}
	\caption{Resultado generando con \Blast para CicloNoDeberiaIrA10}\label{caso5-blast}
\end{figure}

\subsubsection{Caso 6: Ciclo más largo que la cota que hace espuria la instrucción posterior}
En este experimento repetimos la misma idea que en el anterior, terminando el ciclo antes de llegar a su condición de corte, pero haciendo que esto suceda más allá de la cota de recursión de \Corral.

\begin{figure}[H]
    \lstinputlisting[firstline=94, lastline=104]{soundness-experiment/TestCorral.cs}
    \caption{Código del experimento CicloNoDeberiaIrA10ConIf}\label{caso6-codigo}
\end{figure}

Claramente, tener la salida del ciclo en una iteración que no es posible analizar por \Corral altera el resultado que puede concluir del análisis. Como se puede ver en la Figura \ref{caso6-corral} la EPA obtenida es la misma que en todas las demás oportunidades que no logra analizar en profundidad el código. Una sobre-aproximación de la EPA que se pudo obtener con máxima precisión a través del uso de \Blast (Figura \ref{caso6-blast}).

Nuevamente, el hecho de que el ciclo no se complete parece afectar seriamente las conclusiones a las que puede arribar \CodeContracts. La EPA obtenida (Figura \ref{caso6-codecontracts}) es una sobre-aproximación incluso más débil que la que se obtuvo con \Corral.
\clearpage

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/corral/CicloNoDeberiaIrA10ConIf.png}
	\caption{Resultado generando con \Corral para CicloNoDeberiaIrA10ConIf}\label{caso6-corral}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/codecontracts/CicloNoDeberiaIrA10ConIf.png}
	\caption{Resultado generando con \CodeContracts para CicloNoDeberiaIrA10ConIf}\label{caso6-codecontracts}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.8]{soundness-experiment/blast/CicloNoDeberiaIrA10ConIf.png}
	\caption{Resultado generando con \Blast para CicloNoDeberiaIrA10ConIf}\label{caso6-blast}
\end{figure}

\subsubsection{Caso 7: Interacción compleja de estructuras de control de flujo}
Por último decidimos probar una combinación más compleja entre estructuras de control. Intuitivamente, este caso pasa todas las instancias con `Estado' == 0 a `Estado' == 10. Sin embargo, aunque es muy simple para una persona reconocer este comportamiento, nuestras herramientas tienen que poder ser capaces de comprender la dependencia que existe entre el ciclo y el \emph{if}.

\begin{figure}[H]
    \lstinputlisting[firstline=82, lastline=92, classoffset=2,morekeywords={Contract}]{soundness-experiment/TestCorral.cs}
    \caption{Código del experimento CicloDeberiaIrA10ConIf}\label{caso7-codigo}
\end{figure}

Nuevamente \Corral no fue capaz de encontrar esta relación, debido a la longitud del ciclo, aunque de todas formas la EPA obtenida sigue siendo una sobre-aproximación razonable.

\CodeContracts logró un mejor resultado que \Corral, ya que pudo descartar uno de los estados abstractos inalcanzables. Notar que en este caso el ciclo no tiene una salida prematura.

Por último, \Blast puedo concluir que el código se comporta como nosotros esperábamos.
\clearpage

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/corral/CicloDeberiaIrA10ConIf.png}
	\caption{Resultado generando con \Corral para CicloDeberiaIrA10ConIf}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/codecontracts/CicloDeberiaIrA10ConIf.png}
	\caption{Resultado generando con \CodeContracts para CicloDeberiaIrA10ConIf}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{soundness-experiment/blast/CicloDeberiaIrA10ConIf.png}
	\caption{Resultado generando con \Blast para CicloDeberiaIrA10ConIf}
\end{figure}

Como resumen de los comportamientos observados en los diversos casos de estudio podemos aseverar que en todas las oportunidades los resultados de \Corral fueron sound tal como era esperado. De la misma manera, las EPAs generadas tanto con \CodeContracts como \Blast representan sobre-aproximaciones de las que uno puede construir manualmente. 

Claramente los ciclos son un problema para todas las implementaciones. En el caso de \Corral no contar con una cota adecuada para el análisis puede perjudicar considerablemente el resultado del análisis. La sobre-aproximación que utiliza por defecto para caracterizar aquello que no alcanza a analizar no llega a ser suficientemente robusta como para superar determinados obstáculos.

En el caso de \CodeContracts el uso de ciclos causa una variabilidad importante en los resultados. Como pudimos observar, si el ciclo realiza todas las iteraciones hasta alcanzar la condición de la guarda, es capaz de inferir más información que la obtenida por \Corral. Por el contrario, si el ciclo puede terminar en alguna iteración arbitraria, la precisión decrece significativamente consiguiendo resultados peores que en el peor caso de \Corral.

En la mayoría de los casos \Blast es el que obtuvo las mejores abstracciones. A pesar de haber logrado superar la mayoría de los obstáculos los ciclos significan una penalidad en el tiempo necesario para poder analizar el código. Básicamente, el hecho de no contar con una cota que limite el tiempo de exploración facilita la posibilidad de hallar la respuesta correcta a cada query.

Vale la pena destacar que \Corral pudo lograr los mejores resultados en presencia de no determinismo. Debido a que los métodos de las clases suelen tener parámetros, esta característica es prometedora para el uso de \Corral en casos de estudio más complejos y más cercanos a la realidad.