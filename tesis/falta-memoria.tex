Si bien en varias oportunidades logramos obtener una EPA de máxima precisión esto no garantiza que las abstracciones resultantes acepten únicamente las trazas del código. A continuación discutimos algunos de los factores que pueden contribuir a una sobre-aproximación en la EPA.

Recordemos que para obtener una EPA necesitamos tanto del código de la implementación de la API como también de la especificación con los contratos que definen formalmente el comportamiento esperado. Más aún, también podría ser que el model checker no sea lo suficientemente poderoso como para poder concluir al analizar las queries. Por lo tanto, ante la aparición, o ausencia, de una transición sospechosa la tarea de la revisión puede deberse a diversos factores. ¿Es un problema en el código? ¿El contrato no refleja correctamente la semántica de la implementación? ¿El model checker no es lo suficientemente poderoso? ¿El algoritmo de construcción tiene una incidencia significativa? Intentemos analizar cada uno de estos interrogantes.

\subsubsection{Relación código / contratos}
Idealmente, los contratos definen de forma precisa, correcta y completa la semántica del código que representan. De ser así, uno podría determinar la existencia de una transición revisando indistintamente el código o los contratos. Sin embargo, en la práctica esto suele ser muy poco frecuente.

Supongamos que la precondiciones que estamos utilizando son más débiles de lo que en realidad podrían ser. En este caso, dado que el resultado de las queries que testean la validez de una transición (Algoritmo \ref{feasible-transition}) depende del invariante del estado al que llega la transición. Y considerando que el invariante de estado depende de las precondiciones de los métodos habilitados y deshabilitados, tener precondiciones muy débiles contribuyen a que una mayor cantidad de queries resulten como afirmativas y pasen a formar parte del resultado. Esto sucede ya que se analizan únicamente los contratos del objetivo.

En el caso de ser muy fuertes van a imponer mayores restricciones a las potenciales instancias que podrían hacer uso de los métodos y por lo tanto la ausencia de una transición podría deberse a un contrato demasiado restrictivo. En este caso, habría que preguntarse si es que el código contempla casos innecesarios o si la semántica que se pensaba para el método es demasiado reducida.

Por último, tenemos que considerar el invariante de la clase. El invariante de un estado abstracto también se compone del invariante de la clase. Este define una condición global sobre las instancias que representan al tipo y también afecta al testeo de las transiciones, si varias transiciones resultan sospechosas el típico candidato suele ser el invariante.

\subsubsection{Las limitaciones del model checker}
El model checker tiene un rol preponderante en la posibilidad de lograr una buena precisión ya que es quien determina la veracidad de las queries. 

En nuestro caso particular, al utilizar \Corral para analizar código .NET estamos sujetos a una limitación que puede afectar nuestro análisis, el mismo framework .NET. Debido a que \Corral entiende código \Boogie es necesario traducir la implementación de la API, como ya fue mencionado previamente. Sin embargo, para tener un conocimiento completo de la semántica del programa habría que traducir todas aquellas APIs que son utilizadas por el mismo pero que son ajenas a nuestro control. El más usual de estos casos se debe a la interacción con el framework. El no contar con información sobre estas llamadas a métodos ``desconocidos'' hace que para seguir conservando la propiedad de soundness es necesario asumir que cualquier cosa puede pasar en dicha llamada, llevando en un gran porcentaje de estos casos a una explosión de estados perjudicando notablemente el resultado. Un workaround que utilizamos en estos casos es hacer explicito el contrato del método mediante un \emph{assume} y \emph{assert}. De esta manera, forzamos un summary preciso del resultado de la ejecución.
Típicamente la explosión de estados es causado por esta limitación.

\subsubsection{Sobre la precisión de la abstracción}
%Las queries no tienen memoria. Usan de estado a estado, si se conjugan todos los problemas mencionados anteriormente se pierde unformacion creando la ilusion de la existencia de instancias no factibles.
La definición de las EPAs, en particular el método de decisión para agregar transiciones, determina una forma de mirar las transiciones que lleva potencialmente a sobre-aproximaciones debido al agregado de transiciones espurias. Básicamente, la definición asume que solo alcanza con conocer los dos estados involucrados por la transición para determinar si pertenece o no a la abstracción. Esta hipótesis no siempre es cierta como mencionamos previamente. La falta de registro de la historia conlleva a que un estado abstracto pueda representar muchos más estados concretos que a los que realmente se llegaría si se aplicara la secuencia de métodos necesaria para arribar a dicho estado abstracto. Veamos un ejemplo.

\begin{figure}
	\centering
	\lstinputlisting{epas-debiles/Test2.cs}
	\caption{Código con precondiciones débiles\label{fig:codigo-precondiciones-debiles}}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[scale=.21]{epas-debiles/EPA-debil.png}
	\caption{EPA con estados con invariantes débiles\label{fig:epa-invariantes-debiles}}
\end{figure}

Intuitivamente podemos observar que el código de la Figura \ref{fig:codigo-precondiciones-debiles} describe un protocolo lineal. Está pensado para que los métodos se ejecuten de manera secuencial. Sin embargo, como se puede observar en la EPA generada el resultado no es el esperado. Esto se debe a que la precondición del método \textbf{m2} es más débil de lo que podría ser. 

Una forma de mitigar este problema de precisión sería mediante el refuerzo del invariante de la clase para reducir la cantidad de estados posibles considerando la historia. Esto requiere de la revisión manual de la EPA y el protocolo para determinar cuáles son las condiciones necesarias para incorporar.

Otra alternativa para poder recuperar la información perdida es utilizar la técnica de Stratified Inlining implementada en \Corral para ratificar transiciones que pueden ser ejecutadas por alguna instancia o removerlas en caso contrario.

Para este caso ideamos una segunda etapa para el algoritmo BFS que considera a la EPA como si fuese un Control Flow Graph e intenta hacer fallar un assert por cada transición en la EPA. A continuación mostramos el pseudocódigo relacionado con la segunda etapa de análisis.

%\marginpar{Explicar el algoritmo y como correrlo}
\begin{algorithm}[h]
    \caption{Transformar los estados de la EPA en métodos}
    \begin{algorithmic}[1]
        \Procedure{ConvertStatesToMethods}{states:$[State]$}
            \For {each state $s \in states$}
                \State CreateStateMethod(s)
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

\begin{algorithm}[h]
    \caption{Template del método de cada estado}
    \begin{algorithmic}[1]
        \Procedure{S\#\#}{\ }
            \State $a \leftarrow$ Random(0, \#enabledActions($s$))
            \If {$a == 0$}
                \Call {S\#\#\_A}{\ }
            \EndIf
            \If {$a == 1$}
                \Call {S\#\#\_B}{\ }
            \EndIf
            \State ...
            \If {$a == \#$enabledActions($s$)}
                \Call {S\#\#\_K}{\ }
            \EndIf
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

\begin{algorithm}[h]
    \caption{Template del método de estado S01 usando la acción A}
    \begin{algorithmic}[1]
        \Procedure{S01\_A}{\ }
            \State {Create random instances for each parameter ($a_0, a_1, ..., a_n$) of the method A}
            \State A($a_0, a_1, ..., a_n$)
            \For {each state $t \in targets(S01, A)$}
                \If {$inv(target)$}
                    \Assert $<UniqueNumber>$ != id
                    \State S\#$target$()
                \EndIf
            \EndFor
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Si analizamos la EPA de la Figura \ref{fig:epa-invariantes-debiles}, observaremos que al testear la transición, que sabemos que es espuria, \Corral es capaz de probar que no hay errores en el programa. Para todas las demás transiciones es capaz de hacer fallar el assert que identifica a cada transición.

Realizamos pruebas codificando manualmente las EPAs obtenidas de algunos de los ejemplos utilizados previamente logrando resultados satisfactorios.

Quizá lo más interesante es que este algoritmo abre la puerta a la utilización de \Corral para generar una test suite cuyo criterio de cobertura sea la cantidad de transiciones de la EPA. Esto se debe a que si es posible obtener los valores y el orden de ejecución de los métodos de la traza eso nos define un caso de test que garantiza cubrir una transición en particular.