El objetivo de esta sección es presentar un escenario simple y de fácil entendimiento para mostrar cómo el uso de contratos y abstracciones ayuda a la comprensión de un artefacto de software.

Supongamos que estamos intentando desarrollar el software necesario para controlar la típica máquina expendedora de bebidas como la que se observa en la Figura \ref{fig:coca}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=.4]{imgs/vending-coca.jpg}
	\caption{Clásica máquina expendedora\label{fig:coca}}
\end{figure}

El primer paso para el desarrollo es saber cuáles son los aspectos que nos interesan modelar en este caso. 

En un tipo de máquinas como ésta, la expendedora recibe dinero mediante su ranura de billetes para aceptar el pago de los usuarios. Una vez que la cantidad de dinero ingresado es suficiente para realizar la compra, permite seleccionar alguna de las bebidas disponibles. Por último, en aquellos casos en los que el cliente pague con un monto superior al valor de la bebida la máquina devuelve el dinero excedente. Una vez realizados todos estos pasos la máquina se encuentra nuevamente disponible para el próximo cliente.

Una vez que se conocen las acciones que debe poder realizar nuestro programa, podemos pensar en cómo modelar estas operaciones.
Para eso, una alternativa podría ser escribir una clase que permita crear instancias que respondan a los siguientes mensajes:
\begin{itemize}
    \item \textbf{IngresarDinero}: que permite modelar el ingreso de dinero a la máquina y así sumarle crédito al cliente. 
    \item \textbf{LiberarBotella}: que modela el hecho de que el usuario haya seleccionado la bebida que desea tomar y por lo tanto la máquina debe liberar la botella correspondiente.
    \item \textbf{DarVuelto}: en caso de que el dinero ingresado sea superior al valor del producto (que por simplicidad lo consideramos como único para todas las bebidas), se le debe poder indicar a la máquina que devuelva el excedente, para que el cliente solamente pague el valor correspondiente a la botella.
\end{itemize}

En la Figura \ref{fig:codigo-maquina-expendedora}, se puede ver una posible implementación de dicha clase\footnote{Vale la pena aclarar, que hay detalles de implementación que no se muestran ya que no aportan información a nuestro estudio y hacen más compleja la comprensión del ejemplo.}.

\begin{figure}
	\centering
	\lstinputlisting{code-examples/MaquinaExpendedora.cs}
	\caption{Implementación del controlador de la máquina expendedora\label{fig:codigo-maquina-expendedora}}
\end{figure}

Como se puede observar en el código, además de las instrucciones necesarias para representar el progreso en el proceso de la compra, se incluyeron condiciones necesarias para que los métodos puedan ser ejecutados. Estas precondiciones están expresadas en forma de contratos utilizando la biblioteca de \CodeContracts provista por el framework de .NET. Algunas de éstas están definidas de forma explícita por nuestro problema mientras que otras surgieron de la forma en que la solución fue modelada.

Por ejemplo, en el caso de \textbf{IngresarDinero} se espera que el \emph{monto reconocido} por el lector de billetes sea un entero positivo (consecuencia de modelar el dinero como un \emph{int}) y que a su vez no haya otra compra en curso (ya que no tiene sentido el ingreso de dinero del comprador siguiente si el anterior no terminó).

Es importante destacar que suele ser una tarea difícil determinar el conjunto de condiciones necesarias para restringir el uso de los métodos a los casos en los que deberían ser utilizados. Más aún, poder definir dichos casos es un problema en sí mismo y es muy frecuente el hecho de que se cuente con un conocimiento parcial del problema a resolver mientras se intenta crear una solución.

Otro inconveniente muy frecuente es que quien entiende la realidad que se quiere modelar no necesariamente sabe programar, y viceversa. Es decir, existe una distancia entre quien sabe qué es lo que hay que hacer de quien conoce el cómo. En nuestro caso, es posible que el Sr. Coca-Cola no sepa programar, pero sí sabe lo que se espera del funcionamiento de la expendedora. 

Veamos entonces, cómo usando una abstracción podemos intentar salvar esta distancia. Si generamos la abstracción correspondiente al código de nuestro software de control de la máquina expendedora, obtenemos el resultado que se observa en la Figura \ref{fig:epa-maquina-expendedora}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=.37]{imgs/epa-maquina-expendedora.jpg}
	\caption{Abstracción del comportamiento del modelo de la Máquina Expendedora\label{fig:epa-maquina-expendedora}}
\end{figure}

A grandes rasgos, podemos decir que la abstracción obtenida permite visualizar gráficamente los posibles órdenes (o trazas) en los cuales se pueden ir llamando los métodos de la clase, de forma tal que siempre se respeten las precondiciones. En otras palabras, dado que las instancias de la clase pretenden ser una representación computable de la realidad que modelan --- en este caso la máquina expendedora --- y por lo tanto se comportan de la forma en que la máquina física lo hace (ingresar dinero, liberar la botella y dar el vuelto) la Figura \ref{fig:epa-maquina-expendedora} debería permitirnos ``simular'' el comportamiento de la máquina expendedora del mundo real.

Si bien las transiciones en el modelo respetan el nombre que se les otorgó en el código, se puede ver que la mayor parte de la ``programación'' está oculta dentro de la abstracción. Luego, para simular distintos escenarios que la expendedora debería contemplar (y también los que no), basta con ver si existe algún camino de flechas en el diagrama que representan el comportamiento del escenario.

Por ejemplo, podríamos tener el caso en el que se ingresa una cantidad superior de dinero a la necesaria, y por lo tanto, luego de haber liberado la bebida se debe proceder a devolver el vuelto al cliente, como se aprecia en la siguiente figura.

\begin{figure}[H]
	\centering
	\includegraphics[scale=.37]{imgs/epa-maquina-expendedora-ejemplo.jpg}
	\caption{Análisis del caso de la devolución del vuelto\label{fig:ejemplo-epa-maquina-expendedora}}
\end{figure}

A lo largo de la tesis veremos cómo es que se hace para obtener estas abstracciones y algunas de las propiedades que las caracterizan.