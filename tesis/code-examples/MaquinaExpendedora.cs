public enum Bebida { Coca, Light, Zero, Sprite, Agua }
public class MaquinaExpendedora
{
    protected bool ventaEnCurso;
    protected int dineroDisponible;

    [Pure] public bool EstaVendiendo() { return ventaEnCurso; }
    [Pure] public int DineroDisponible() { return dineroDisponible; }
    [Pure] public int PrecioDeLaBotella() { return 15; }

    public MaquinaExpendedora()
    {
        ventaEnCurso = false;
        dineroDisponible = 0;
    }

    public void IngresarDinero(int montoReconocido)
    {
        Contract.Requires(montoReconocido > 0);
        Contract.Requires(!EstaVendiendo());

        dineroDisponible += montoReconocido;
    }

    public void LiberarBotella(Bebida bebidaSeleccionada)
    {
        Contract.Requires(!EstaVendiendo());
        Contract.Requires(DineroDisponible() >= PrecioDeLaBotella());

        ventaEnCurso = true;
        dineroDisponible -= PrecioDeLaBotella();

        // Dejar caer una botella de la bebidaSeleccionada

        if (dineroDisponible == 0)
            ventaEnCurso = false;
    }

    public void DarVuelto()
    {
        Contract.Requires(EstaVendiendo());
        Contract.Requires(DineroDisponible() > 0);

        dineroDisponible = 0;
        ventaEnCurso = false;
    }
}
