Una opción mucho más eficiente es la presentada en \cite{DeCaso2011}, cuyo pseudocódigo se exhibe en el Algoritmo \ref{algoritmo-bfs}. La idea detrás de este método es realizar una exploración de búsqueda a lo ancho, o simplemente BFS por sus siglas en inglés, del espacio
de estados.

\begin{algorithm}
    \caption{Template para la construcción BFS de una EPA}
    \label{algoritmo-bfs}
    \begin{algorithmic}[1]
        \Procedure{ConstructEPA}{C: $\langle A, F, R, inv, init \rangle$}\ : $\langle \Sigma, S, S_0, \delta \rangle$
            \State $\Sigma \leftarrow A$
            \State $S \leftarrow \emptyset$
            \State $\delta(as, a) \leftarrow \emptyset \ \forall as, a$
            \State $A^- \leftarrow \{ a \in A\ |\ \forall\ c.\ init(c) \Rightarrow \neg \exists p.\ R_a(c,p)\}$
            \State $A^+ \leftarrow \{ a \in A\ |\ \forall\ c.\ init(c) \Rightarrow \exists p.\ R_a(c,p)\}$
            \State $S_0^C \leftarrow \{ as \subseteq A\ |\ A^+ \subseteq as, A^- \cap as = \emptyset \}$
            \State $S_0 \leftarrow \left\{ as \in S_0^C\ |\ \exists c.\ inv_{as}(c) \land init(c) \right\}$
            \State $W \leftarrow \text{queue starting with elements in}\ S_0$
            \While {there is a certain $as$ as the head of $W$}
                \State $W \leftarrow W - [as]$
                \State $S \leftarrow S \cup \{as\}$
                \For {each action $a \in as$}
                    \State $B^- \leftarrow \left\{ b \in A\ |\ \forall c,p.\ inv_{as}(c) \land R_a(c,p) \Rightarrow \neg \exists p'.\ R_b(f_a(c,p), p') \right\}$
                    \State $B^+ \leftarrow \left\{ b \in A\ |\ \forall c,p.\ inv_{as}(c) \land R_a(c,p) \Rightarrow \exists p'.\ R_b(f_a(c,p), p') \right\}$
                    \State $S^C \leftarrow \left\{ bs \subseteq A\ |\ B^+ \subseteq bs, B^- \cap bs = \emptyset \right\}$
                    \For {each state $bs \in S^C$}
                        \If {($\exists c.\ inv_{as}(c) \land \exists p.\ R_a(c,p) \land inv_{bs}(f_a(c,p))$)}
                            \State $\delta(as, a) \leftarrow \delta(as,a) \cup \{bs\}$
                            \If {$bs \notin S \land bs \notin W$}
                                \State $W \leftarrow W \cup [bs]$
                            \EndIf
                        \EndIf
                    \EndFor
                \EndFor
            \EndWhile
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Para construir la EPA vamos a partir con una función de transición ($\delta$) y un conjunto de estados ($S$) inicializados como vacíos.

El conjunto $A^-$ contiene todos los métodos que no pueden estar habilitados en los estados iniciales, debido a que sus precondiciones nunca se cumplen luego de crear una instancia de la clase. De forma análoga, el conjunto $A^+$ contiene todos los métodos que siempre están habilitados en todos los estados iniciales.

Habiendo constituido los conjuntos de acciones $A^+$ y $A^-$ es posible definir el conjunto de estados candidatos para ser iniciales $S_0^C$. Estos estados se caracterizan por contener todos los métodos de $A^+$ y ninguno de $A^-$, notar que no necesariamente vale que $A^+ \cup A^- = A$.

A partir de los candidatos se definen los estados iniciales como aquellos para los cuales existan instancias que verifiquen la condición inicial ($init$) así como también el correspondiente invariante de estado. De esta forma, conseguimos que el conjunto de estados iniciales cumpla con el punto 3 de la caracterización definida previamente. Naturalmente, mientras más métodos sean clasificados como necesariamente habilitados o deshabilitados menor será la cantidad de estados iniciales candidatos que deberán ser verificados posteriormente.

Una vez determinado $S_0$ se emplea BFS para continuar con la construcción de la EPA. Para eso se inicializa una cola $W$ con los estados pendientes de ser analizados, en otras palabras con $S_0$. 

Cada vez que un estado $as$ pasa a ser analizado se lo agrega al conjunto de estados de la abstracción, ya que el hecho de estar en la cola lo hace alcanzable. Recordemos que el estado $as$ representa a todas las instancias que únicamente pueden ejecutar los métodos de $as$. Por lo tanto, vamos a ver a qué nuevos estados podemos llegar al ejecutar alguna de la acciones habilitadas $a \in as$.

De forma similar a la etapa de inicialización del algoritmo, el conjunto $B^-$ contiene todas las acciones que nunca están habilitados luego de ejecutar $a$ a partir del estado $as$. En otras palabras, aquellos métodos cuyas precondiciones nunca se cumplen luego de ejecutar $a$. Análogamente, el conjunto $B^+$ contiene todos los métodos que siempre están habilitados al ejecutar $a$.

Al igual que antes construimos el conjunto de estados potencialmente alcanzable $S^C$, los cuales se caracterizan por contener todos los métodos de $B^+$ y ninguno de $B^-$.

Posteriormente, cada uno de estos estados candidato es considerado para verificar si puede ser alcanzado realmente al evolucionar instancias de $as$ tras la ejecución del método $a$. Por último, cada vez que un nuevo estado abstracto $bs$ es alcanzado, se lo encola en $W$ para ser analizado posteriormente.

La demostración de correctitud de este algoritmo fue realizada en el trabajo previo antes citado \cite{DeCaso2011}. 

Cabe mencionar que la complejidad temporal y espacial en el peor caso sigue siendo exponencial respecto de la cantidad de métodos. Sin embargo, la diferencia radica en que mientras podamos clasificar más métodos como necesariamente habilitados o deshabilitados en un estado en particular, menos estados candidatos deben ser considerados por el algoritmo. En la práctica, esta optimización reduce de forma significativa los tiempos de ejecución.