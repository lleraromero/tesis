El primer paso en la integración de todos los componentes desarrollados en capítulos anteriores, es analizar en qué partes del algoritmo de creación de EPAs descripto en el Algoritmo \ref{algoritmo-bfs}, es necesaria la intervención de \Corral.

%\marginpar{Acordarse de escribir sobre las reachability queries en el algoritmo (o aca)}
El algoritmo tiene una naturaleza de exploración, esto quiere decir que la idea detrás del mismo es ir descubriendo los estados y transiciones que componen a la EPA. Para esto primero se necesita tener un mecanismo que permita, a partir de un estado cualquiera `s', determinar cuáles son los estados potencialmente alcanzables y luego en una etapa posterior verificar cuáles de las potenciales transiciones son efectivamente válidas. 

Para la primera de las preguntas, el método es separar las acciones de la clase bajo análisis en dos conjuntos que son utilizados para encontrar estados potencialmente alcanzables. Para poder determinar qué elementos pertenecen a cada conjunto la propuesta en este caso es utilizar \Corral. Dependiendo de cuál sea el conjunto a determinar vamos a recurrir a diferentes \emph{queries} de \emph{reachability} para caracterizarlo.

\subsubsection{Determinar estados potencialmente alcanzables}
De los dos conjuntos requeridos el más importante es el conjunto de las acciones habilitadas. Este caracteriza la respuesta a la pregunta: \emph{¿qué acciones quedan \textbf{necesariamente} habilitadas si estando en un estado `s' ejecuto la acción `a'?}. 

Análogamente, el algoritmo sugiere encontrar el conjunto de acciones deshabilitadas ya que estas permiten recortar el espacio de búsqueda.

Dicha clasificación se logra evaluando cada acción de la clase mediante lo que llamaremos una query positiva y otra negativa.
Las queries positivas intentan probar que desde `\emph{s}' ejecutando `\emph{a}' siempre se puede probar que es válida la precondición de `\emph{b}', o en otras palabras, que la acción `\emph{b}' pertenece al conjunto de acciones habilitadas. 

Formalmente, lo que se intenta encontrar es el siguiente conjunto de acciones:
\begin{equation*}
B^+ = \{\ b \in A\ |\  \forall c, p.\ inv_s(c) \land R_a(c,p) \Rightarrow \exists p'. R_b(f_a(c,p), p') \}
\end{equation*}

%\marginpar{Explicamos reachability antes?}
Debido a que en .NET se cuenta con la biblioteca \CodeContracts, en \cite{ZoppiBCGU11} se optó por reescribir las \emph{queries} descriptas en \cite{DeCaso2011} con el objetivo de hacer uso de la biblioteca y de \Clousot para poder determinar si la acción cumplía con la condición deseada. Valiéndonos de esta modificación nuestro primer enfoque fue el de utilizar las mismas \emph{queries} como input de \Corral. A modo de ejemplo, se puede observar en el Algoritmo \ref{enabled-query-codecontracts} el esquema de una query positiva.

\begin{algorithm}
    \caption{Query para determinar si una acción debe estar habilitada usando \CodeContracts}
    \label{enabled-query-codecontracts}
    \begin{algorithmic}
        \Procedure{MustBeEnabledAction}{$s:\text{State}, a, b : \text{Action}$}
            \Requires inv(s)
            \Ensures pre(b)
            \State \textbf{call} a()
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Sin embargo, tal como están armadas las queries no es posible usarlas directamente con \Corral. El primer obstáculo surge debido a las tecnologías empleadas, tal como se menciona en la sección \ref{ch:corral} la entrada que recibe \Corral es un programa escrito en \Boogie. Por lo tanto, el primer paso necesario para poder utilizarlo consiste en traducir las queries escritas en .NET junto con sus respectivos contratos a código \Boogie.

Para realizar la traducción del código recurrimos al traductor de bytecode de .NET a \Boogie llamado BytecodeTranslator\footnote{Disponible en: \url{https://github.com/boogie-org/bytecodetranslator}}. Lamentablemente, al momento de escribir esta tesis el traductor no es capaz de traducir los contratos escritos para .NET en contratos que decoren el código escrito en \Boogie, aunque este último permita el uso de los mismos. Como consecuencia, es necesario modificar las queries generadas para poder traducir los contratos de forma satisfactoria. Para que las precondiciones y postcondiciones estén presentes en el código escrito en \Boogie, se procedió al reemplazo de los \textbf{requires} y \textbf{ensures} por \textbf{assume} y \textbf{assert} respectivamente, como se puede apreciar en el Algoritmo \ref{enabled-query}. Notar, que también el orden de las instrucciones debe cambiar debido a que las postcondiciones, en forma de asserts, deben ser evaluadas al finalizar la ejecución de la query.

\begin{algorithm}
    \caption{Query para determinar si una acción debe estar habilitada}
    \label{enabled-query}
    \begin{algorithmic}
        \Procedure{MustBeEnabledAction}{$s:\text{State}, a, b : \text{Action}$}
            \Assume inv(s)
            \State \textbf{call} a()
            \Assert pre(b)
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Una vez que la traducción de la clase bajo análisis junto con las queries está terminada, se realiza una llamada a \Corral por cada una de las queries. Esto se realiza de esta manera ya que \Corral necesita que se le especifique un único \emph{entrypoint} a partir del cual empezar a analizar cada vez que es ejecutado. 

Es interesante notar que acá hay una diferencia con el uso de \CodeContracts como \emph{solver} ya que este último requiere de una única llamada para analizar toda una clase. En este sentido, \CodeContracts actúa como caja negra para nuestro algoritmo y resulta más complejo tener un control más fino de la forma de resolución. Aunque éste cuenta con la posibilidad de verificar los métodos en forma paralela, es necesario especificarle a \CodeContracts qué es lo que tiene que verificar ya que por defecto analiza todo el \emph{assembly} recibido como entrada. Esto conlleva una penalización de performance que puede ser considerable teniendo en cuenta lo potencialmente grande que puede ser el espacio de búsqueda.

Como \Corral es ejecutado como un proceso externo y dado que el resultado de la ejecución es mostrado por consola, se realiza un proceso de \emph{parseo} del output del análisis de forma de obtener la conclusión a la que se arribó. Al contener un algoritmo de semi-decisión, los resultados a los que se puede llegar al analizar una query son:
\begin{itemize}
    \item ``True bug'': se encontraron valores para las variables con los cuales halló una traza que hace fallar un \emph{assert}
    \item ``No bugs found'': \Corral pudo probar que no hay ejecución posible que haga fallar los \emph{asserts}.
    \item ``Reached Recursion Bound'': no se encontraron bugs, sin embargo, tampoco se puede concluir que no existan. Esto se debe a que se alcanzó la cota de recursión que se está dispuesto a intentar.
\end{itemize}

Para poder preservar la semántica definida en \cite{DeCaso2011} al utilizar \Corral para ejecutar las queries de reachability, vamos a reinterpretar los resultados de la siguiente manera:
\begin{itemize}
\item ``True bug'' $\rightarrow$ Target reached
\item ``No bugs found'' $\rightarrow$ Target unreachable
\item ``Reached Recursion Bound'' $\rightarrow$ Target may be reachable
\end{itemize}

De esta forma, de acuerdo al algoritmo de construcción de EPAs de \cite{DeCaso2011} se utilizan las siguientes reglas para determinar si una acción pertenece al conjunto de `habilitadas':
\begin{itemize}
    \item Target Reached: no se agrega la acción al conjunto de habilitadas.
    \item Target may be reachable: no se la agrega al conjunto de habilitadas y se deja asentado la imposibilidad de \Corral de concluir un resultado.
    \item Target Unreachable: se agrega al conjunto de habilitadas.
\end{itemize}

Considerando las conclusiones de \Corral de esta manera nos permite redefinir las queries, que inicialmente estaban pensadas para \Blast, para incorporar el enfoque híbrido de \Corral.

Análogamente, como fue mencionado anteriormente también se utilizan queries negativas. Estas son las que intentan ver que la negación de la precondición de `\emph{t}' es cierta, sin importar los valores de las variables involucradas. En otras palabras, las acciones que seguro no van a poder ser ejecutadas. Formalmente,
\begin{equation*}
B^- = \{\ b \in A\ |\  \forall c, p.\ inv_s(c) \land R_a(c,p) \Rightarrow \neg \exists p'. R_b(f_a(c,p), p') \}
\end{equation*}

Para las queries negativas utilizamos un esquema muy similar al de las positivas, tal como se puede ver en el Algoritmo \ref{disabled-query}. La única diferencia se puede observar en la condición que vamos a intentar analizar si es alcanzable o no, en este caso, la negación de la precondición de la acción `$b$'.

\begin{algorithm}
    \caption{Query para determinar si una acción debe estar deshabilitada}
    \label{disabled-query}
    \begin{algorithmic}
        \Procedure{MustBeDisabledAction}{$s:\text{State}, a, b : \text{Action}$}
            \Assume inv(s)
            \State \textbf{call} a()
            \Assert not(pre(b))
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

De esta manera, nos quedan definidos tres conjuntos disjuntos de acciones, las que siempre están \textit{habilitadas}, las que necesariamente están \textit{deshabilitadas} y el resto de las acciones, que no pertenecen a ninguno de los conjuntos anteriores. Esto nos permite resolver las instrucciones 5, 6, 14 y 15 del Algoritmo \ref{algoritmo-bfs} de creación de EPAs. De esta forma, nos es posible definir todos los posibles estados alcanzables desde `$s$'.

\subsubsection{Testear los estados candidatos}
La segunda etapa que requiere de la intervención de \Corral es cuando se quiere responder la pregunta: de todos los estados candidatos ¿cuáles son efectivamente alcanzables si estoy en el estado `$s$' y ejecuto la acción `$a$'?

Al igual que en la etapa anterior vamos a recurrir al uso de queries de reachability para encontrar la respuesta. En este caso, se crea una sola query negativa por cada tupla de estado origen, acción a ejecutar y estado destino. El esquema utilizado para estas consultas es el descripto en el Algoritmo \ref{feasible-transition}.

\begin{algorithm}
    \caption{Query para determinar si una transición debe ser agregada a la EPA}
    \label{feasible-transition}
    \begin{algorithmic}
        \Procedure{FeasibleTransition}{$s:\text{State}, a:\text{Action}, t:\text{State} $}
            \Assume inv(s)
            \State \textbf{call} a()
            \Assert $\neg$inv(t)
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Utilizando el mapeo semántico descripto anteriormente, el resultado de la ejecución de \Corral es interpretado siguiendo las reglas enumeradas a continuación:
\begin{itemize}
    \item Target Reached: se considera a la transición como válida y por lo tanto se la agrega al EPA.
    \item Target May be reachable: la transición se la supone posible, ya que no hay un resultado concluyente, y se la agrega al EPA. Debido a la falta de certeza se la denota con un `?'.
    \item Target Unreachable: nunca vale el invariante del estado destino utilizando la acción $a$, por lo tanto, no se agrega la transición al EPA.
\end{itemize}

Una vez que todas las queries fueron ejecutadas y sus resultados obtenidos de la salida de la consola, se cuenta con un subconjunto de los estados candidatos obtenidos previamente. De esta manera, se cumple con las instrucciones 8 y 18 del Algoritmo \ref{algoritmo-bfs}, agregando todas las transiciones necesarias a la EPA junto con los nuevos estados descubiertos.

\subsubsection{Extendiendo las queries para incorporar el soporte de parámetros}
Hasta este punto se trabajó con la suposición de que las acciones no hacían uso de parámetros. Esta suposición propone una simplificación muy importante en las queries. Permitir la inclusión de parámetros requiere de un tratamiento particular de las condiciones ya que algunas de estas no pueden ser representadas como queries de reachability. Esto se debe a que su validez requiere poder construir una función que devuelva el valor de los parámetros para cada posible instancia. En \cite{DeCaso2011} se propone una solución que requiere de la inclusión de predicados por parte del programador. Básicamente, la inclusión de estos predicados evitan el uso de parámetros mediante nuevas queries que usan sobre-aproximaciones y sub-aproximaciones de precondiciones. Nosotros optamos por un enfoque intermedio que permite no perder tanta precisión al utilizar parámetros bajo determinadas condiciones y que a la vez no utiliza los predicados propuestos en \cite{DeCaso2011}, ya que se delega la posibilidad de encontrar dichas funciones en el \emph{model checker}.

\subsubsection{Ejemplo 3}
Veamos primero cuál es el problema que surge al utilizar parámetros en las queries. 

Supongamos que estamos intentando determinar si la acción \textbf{IngresarDinero} de la clase \textbf{MaquinaExpendedora} está siempre habilitada luego de ejecutar \textbf{DarVuelto}, partiendo de un estado $s$. Tal como se describió anteriormente, la correspondiente query positiva sería como la que se puede visualizar en el Algoritmo \ref{ejemplo-query}.
\begin{algorithm}
    \caption{Ejemplo de query positiva con parámetros}
    \label{ejemplo-query}
    \begin{algorithmic}
        \Procedure{MustBeEnabledAction}{$s:\text{State}, DarVuelto, IngresarDinero : \text{Action}$}
            \Assume inv(s)
            \State \textbf{call} DarVuelto()
            \Assert montoReconocido > 0 $\land$ !EstaVendiendo()
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

Debido a que \emph{montoReconocido} es un parámetro, el predicado \emph{montoReconocido} >\ 0 del assert de la query no puede ser afectado por ninguna de las instrucciones anteriores. Por lo tanto, \emph{montoReconocido} es una variable libre en el contexto del \emph{assert} y es el camino más fácil que tiene \Corral para poder encontrar un bug en el código. Simplemente, hallando algún valor para los parámetros que hagan falso el assert, es suficiente para que la query tenga un error. Luego, siempre que exista un parámetro las queries darán como resultado ``Target reached'' y de esta manera anulan cualquier tipo de información que se pudiera obtener de ellas. Es importante notar que la pérdida de información no afecta el hecho de que la EPA sea una sobre-aproximación, aunque si puede afectar la precisión de la misma. Sin embargo, estas queries tienen un impacto mucho más significativo en la performance. Perder información obliga a tener que generar más consultas para poder testear todas las posibles transiciones de la EPA. Dado que las llamadas al model checker son costosas, tener una mayor cantidad de consultas que realizar impacta seriamente en el tiempo necesario para poder resolverlas.

Para poder solucionar este problema vamos dividir a las precondiciones en dos categorías, las que predican únicamente sobre los parámetros y las que incluyen referencias a variables de la instancia. Para una clasificación más detallada, más precisa, pero dependiente de anotaciones referirse a \cite{DeCaso2011}.

Para las precondiciones que afectan exclusivamente a los parámetros vamos a asumir que existe una valuación de los mismos que las hacen verdaderas. Esta suposición se basa en que los contratos de todos los métodos deben ser satisfactibles. De todas formas, aunque estos no lo fueran, es fácilmente verificable a través de otra query de reachability.

Teniendo esto en cuenta, reescribimos las consultas de modo tal que los asserts que hagan únicamente referencia a precondiciones de parámetros pasen a ser assumes. De esta manera, \Corral no podrá encontrar un error en parámetros que no cumplan la precondición y solo podrá encontrarlos en condiciones que afecten a las variables internas de la instancia.
Luego, suponiendo que existen valores para los parámetros la query queda reescrita de la siguiente manera:
\begin{algorithm}
    \caption{Query extendida al uso de parámetros para determinar si una acción debe estar habilitada}
    \label{enabled-query-alternativa}
    \begin{algorithmic}
        \Procedure{MustBeEnabledAction}{$s:\text{State}, a, b : \text{Action}$}
            \Assume inv(s)
            \State \textbf{call} a()
            \Assume pre\_de\_param(b) \Comment los requieres de parámetros que son independientes del estado de la clase.
            \Assert pre\_de\_variables\_internas(b) \Comment los requieres que hacen referencia a la comparación del estado con los parámetros y controles sobre el estado mismo.
        \EndProcedure
    \end{algorithmic}
\end{algorithm}