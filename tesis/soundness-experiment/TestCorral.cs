using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;

namespace Examples
{
    class TestCorral
    {
        public int Estado;

        public TestCorral()
        {
            Estado = 0;
        }

        /// <summary>
        /// Deberia llevarme solamente al TestigoEstado10, ya que siempre parte de 0 y llega a 10 con el loop.
        /// </summary>
        public void CicloMasLargoQueRecursionBound()
        {
            Contract.Requires(Estado == 0);

            for (int i = 0; i < 10; i++)
            {
                Estado++;
            }
        }

        public void CicloMasChicoQueRecursionBound()
        {
            Contract.Requires(Estado == 0);
            for (int i = 0; i < 1; i++)
            {
                Estado++;
            }
        }

        public void TestigoEstado1()
        {
            Contract.Requires(Estado == 1);
        }

        public void TestigoEstado10()
        {
            Contract.Requires(Estado == 10);
        }

        public void CicloBasadoEnParam(int cota)
        {
            Contract.Requires(Estado == 0);

            for (int i = 0; i < cota; i++)
            {
                Estado++;
            }
        }

        public void CicloDeberiaIrA10()
        {
            Contract.Requires(Estado == 0);

            for (int i = 0; i < 30; i++)
            {
                Estado++;
            }
            Estado = 10;
        }

        public void CicloNoDeberiaIrA10()
        {
            Contract.Requires(Estado == 0);

            for (int i = 0; i < 30; i++)
            {
                return;
            }
            Estado = 10;
        }

        public void CicloDeberiaIrA10ConIf()
        {
            Contract.Requires(Estado == 0);

            for (int i = 0; i < 30; i++)
            {
                Estado++;
            }
            if (Estado == 30)
                Estado = 10;
        }

        public void CicloNoDeberiaIrA10ConIf()
        {
            Contract.Requires(Estado == 0);

            for (int i = 0; i < 30; i++)
            {
                if (i == 10)
                    return;
            }
            Estado = 10;
        }
    }
}
