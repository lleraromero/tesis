public class Test2
{
    public int x;
    public int y;

    public Test2()
    {
        x = 0;
        y = 0;
    }

    public void m1()
    {
        Contract.Requires(x == 0 && y == 0);
        x = 5;
    }

    public void m2()
    {
        Contract.Requires(x > 0 /* x == 5 */ && y == 0);
        y = 7;
    }

    public void m3()
    {
        Contract.Requires(x == 5 && y == 7);
        x = 3;
        y = 3;
    }        
}