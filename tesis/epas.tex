Antes de presentar los algoritmos que permiten construir de forma automática la abstracción, es necesario introducir primero los aspectos formales que los fundamentan así como también establecer una notación clara para los mismos. Para mayor detalle e información al respecto consultar \cite{DeCaso2011}, trabajo en el cual nos basamos.

\subsection{Modelo formal}
El objeto de análisis para nuestra propuesta es el código fuente de una clase. Por lo tanto, es necesario como primer paso definir la interpretación semántica del mismo.

Para esto vamos a considerar a una clase $C$ como una estructura de la forma $\langle M, F, R, inv, init\rangle$, donde:
\begin{enumerate}
    \item $M = \{m_1, \dots, m_n\}$ es un conjunto finito de etiquetas de los métodos que saben responder las instancias de la clase
    \item $F$ es el conjunto de implementaciones de los métodos (\textit{functions}) indexado por $m \in M$
    \item $R$ es el conjunto de precondiciones (\textit{requires clauses}) indexado por $m \in M$
    \item $inv$ es el invariante de la clase
    \item $init$ es la condición inicial, que indica cuáles son las instancias iniciales de la clase
\end{enumerate} 

\subsubsection{Ejemplo 1}
Veamos como se aplica este modelo al ejemplo presentado previamente. Recordando, los métodos que expone la clase \textbf{MaquinaExpendedora} son: \emph{IngresarDinero}, \emph{LiberarBotella}, \emph{DarVuelto}.

Luego, la interpretación semántica del ejemplo bajo el modelo formal sería:
\begin{equation*}
MaquinaExpendedora = \langle M, F, R, inv, init \rangle \text{ donde,}
\end{equation*}

\begin{itemize}
    \item[] $M = \left \{IngresarDinero, LiberarBotella, DarVuelto\right \}$
    \item[] $F = \left \{f_{IngresarDinero}(montoReconocido), f_{LiberarBotella}, f_{DarVuelto} \right \}$
    \item[] $R = \left \{R_{IngresarDinero}(montoReconocido), R_{LiberarBotella}, R_{DarVuelto} \right \}$
    \begin{itemize}
        \item[]$
            \begin{aligned}
                R_{IngresarDinero}(montoReconocido) &= \text{montoReconocido} > 0 \land \text{!EstaVendiendo()}\\
                                   &\equiv \text{montoReconocido} > 0 \land \text{!ventaEnCurso}
            \end{aligned}
            $
        \item[]$
            \begin{aligned} 
                R_{LiberarBotella} &= \text{!EstaVendiendo()} \land \text{DineroDisponible()} \geq \text{PrecioDeLaBotella()} \\
                                   &\equiv \text{!ventaEnCurso} \land \text{dineroDisponible} \geq 15
            \end{aligned}
            $
        \item[]$
            \begin{aligned}  
                R_{DarVuelto} &= \text{EstaVendiendo()} \land \text{DineroDisponible()} > 0 \\
                              &\equiv \text{ventaEnCurso} \land \text{dineroDisponible} > 0
            \end{aligned}
            $
    \end{itemize}
    \item[] $inv$ = true
    \item[] $init$ = !ventaEnCurso $\land$ dineroDisponible = 0
\end{itemize}

¿Por qué no aparecen los métodos ``constructores'' en el modelo? El lector perspicaz pudo haber notado que si bien en la EPA usada como motivación aparecen transiciones con los constructores de la clase estos no aparecen en el modelo formal. Es fácil ver que debido a que los métodos de construcción de instancia no son mensajes que puedan responder las instancias de la clase estos no pertenecen a la EPA de la clase. Sin embargo, en lenguajes como C\# o Java en donde existe una mezcla entre el paradigma imperativo y el orientado a objetos, el programador define cómo se crea una instancia y qué mensajes sabe responder en el mismo archivo de texto. Es por este motivo que nos parece apropiado hacer este abuso de notación con el objetivo de hacer más relevante la información que aporta la abstracción.

Más aún, como veremos más adelante los constructores tienen un tratamiento especial en el algoritmo de creación de EPAs.

Lo siguiente que debemos definir es el espacio de estados posibles, el cual se caracteriza mediante un sistema infinito y determinístico de transiciones etiquetadas (\textit{labelled transition system} o LTS). Se define un LTS como una estructura de la forma $\langle\Sigma, S, S_0, \delta\rangle$, donde $\Sigma$ es el conjunto de etiquetas, $S$ es el conjunto de estados, $S_0 \subseteq S$ es el conjunto de estados iniciales y $\delta: S \times \Sigma \rightarrow S$ es una función de transición parcial.

De esta forma, el espacio de estados posibles correspondiente a la interpretación semántica de una clase está compuesto por un estado por cada instancia válida (es decir, que cumple el invariante de clase) y un estado inicial que cumple con la condición inicial. Luego, para cada estado, $s_i$ correspondiente a una instancia válida que cumple con la precondición de algún método $m$ existe una transición de etiqueta $m$ a otro estado $s_j$ correspondiente a la instancia resultante luego de aplicar $m$ en caso de ser válida. Es importante notar que el espacio de estados recién definido sólo tiene en cuenta las instancias que cumplen con el invariante de clase. Para una definición formal ver \cite{DBGU13}.

Ahora que tenemos definido el espacio de estados posibles (recordar que es infinito) debemos establecer un nivel de abstracción adecuado para obtener una representación finita del mismo, la cual podamos generar y manipular. La experiencia indica que agrupar los estados concretos en los cuales se encuentran habilitados el mismo conjunto de métodos es un nivel de abstracción que provee una buena relación entre tamaño y precisión. Esto se debe a que brinda buena trazabilidad con el código ya que se puede navegar a los métodos y mirar las precondiciones.

Por lo tanto es necesario formalizar esta noción de equivalencia de instancias. Dada una clase $C$ y dos instancias $c_1, c_2 \in C$ decimos que $c_1$ y $c_2$ son equivalentes respecto de los métodos que habilitan, es decir \textit{enabledness equivalent} (lo notaremos $c_1 \equiv_e c_2$), si y solo si para cada método $m \in M$ vale $R_m(c_1) \Leftrightarrow R_m(c_2)$.

Dado el LTS del espacio de estados posibles correspondiente a la interpretación semántica de una clase podemos definir un tipo de abstracción denominada \textit{enabledness-preserving} (o simplemente EPA) como una máquina de estados no determinística que agrupa las instancias de la clase según los métodos que se encuentran habilitados. Dicha abstracción es capaz de simular cualquier traza del LTS original. Nuevamente remitirse a \cite{DeCaso2011} para una definición formal. 

En otras palabras, el conjunto infinito de instancias de una clase particionado mediante la noción de equivalencia $\equiv_e$ antes definida, resulta en un conjunto finito de estados abstractos tales que cada uno de ellos corresponde a un grupo distinto de métodos habilitados. Es decir, cada estado abstracto agrupa a todas las instancias que comparten el mismo conjunto de métodos habilitados y pueden ser caracterizados con un \textit{invariante de estado}.

Dicho invariante hace posible la construcción de este tipo de abstracciones, debido a que si bien desde un punto de vista teórico podemos obtener el EPA a partir del LTS utilizando el concepto de equivalencia de instancias descripto previamente, en la práctica esto no es posible dado que el LTS es infinito. Por lo tanto, es necesario recurrir a algún mecanismo que permita generar EPAs directamente del código fuente sin tener que considerar previamente su correspondiente espacio de estados concreto.

Dada una clase $C = \langle M, F, R, inv, init\rangle$ se define el invariante de un estado abstracto dado por un conjunto de métodos $ms \subseteq M$ como el predicado $inv_{ms}: C \rightarrow \{true, false\}$,
\begin{equation*}
inv_{ms}(c) \xLeftrightarrow{def} inv(c)\ \land \bigwedge \limits _{m \in ms}  R_m(c)\ \land \bigwedge \limits _{m \notin ms} \neg R_m(c) 
\end{equation*}

De esta definición se desprende que un estado abstracto $ms$ es válido si y solo si $\exists c \in C.\ inv_{ms}(c)$. Por lo tanto, un estado abstracto es simplemente un conjunto de métodos; que en caso de ser válido, existe una instancia $c \in C$ en la que se encuentran habilitadas (y ningún otro método lo está, es decir, todos los otros métodos que no pertenecen a este conjunto se encuentran deshabilitados en esa misma instancia $c$).

\subsubsection{Ejemplo 2}
Usando la definición del invariante de estado en el ejemplo de la máquina expendedora, los invariantes para cada estado que aparece en la abstracción serían los siguientes:

\begin{itemize}
    \item[]$
    \begin{aligned}
    inv_{\left\{ IngresarDinero \right\}}(c) \Leftrightarrow &\ inv(c) \land R_{IngresarDinero}(c, m) \land \neg R_{LiberarBotella}(c)  \\ &\land \neg R_{DarVuelto}(c)\\
                                             \Leftrightarrow &\ true\ \land\ (\exists m.\ m > 0\ \land\ !c.ventaEnCurso)\ \land
                                                            \\ &\neg (!c.ventaEnCurso\ \land\ c.dineroDisponible \geq 15)\ \land
                                                            \\ &\neg (c.ventaEnCurso \land c.dineroDisponible > 0)\\
                                             \Leftrightarrow & \ (\exists m.\ m > 0\ \land\ !c.ventaEnCurso)\ \land
                                                            \\ &(c.ventaEnCurso\ \lor\ c.dineroDisponible < 15)\ \land
                                                            \\ &(!c.ventaEnCurso \lor c.dineroDisponible \leq 0)\\
                                             \Leftrightarrow & \ (\exists m.\ m > 0\ \land\ !c.ventaEnCurso)\ \land c.dineroDisponible < 15
    \end{aligned}
    $
    \item[]$
    \begin{aligned}
    inv_{\left\{ \substack{IngresarDinero\\LiberarBotella} \right\}}(c) \Leftrightarrow &\ inv(c) \land R_{IngresarDinero}(c, m) \land R_{LiberarBotella}(c) \\ &\land \neg R_{DarVuelto}(c) \\
                                                                        \Leftrightarrow &\ (\exists m.\ m > 0\ \land\ !c.ventaEnCurso)\ \land\ c.dineroDisponible \geq 15
    \end{aligned}
    $
    
    \item[]$
    \begin{aligned}
    inv_{\left\{ DarVuelto \right\}}(c) \Leftrightarrow &\ inv(c) \land R_{DarVuelto}(c) \land \neg R_{IngresarDinero}(c, m) \land \neg R_{LiberarBotella}(c)\\
                                        \Leftrightarrow &\ c.ventaEnCurso \land c.dineroDisponible > 0
    \end{aligned}
    $
\end{itemize}

Habiendo caracterizado formalmente los estados abstractos lo único que resta definir son las transiciones de la abstracción.

Vamos a decir que dos estados $ms$ y $ms'$ estarán conectados mediante una transición con etiqueta $m$ si existe una instancia $c$ que evoluciona en $c'$ luego de ejecutar el método $m$. Además hay que tener en cuenta que debe valer que en $ms$ sea válida la precondición de $m$ y que $c'$ satisfaga el invariante de $ms'$.

Ahora si estamos en condiciones de caracterizar una EPA. Diremos que una EPA es una estructura $\langle \Sigma, S, S_0, \delta \rangle$ a partir de una clase dada $C = \langle M, F, R, inv, init \rangle$ que cumple las siguientes condiciones:
\begin{enumerate}
    \item $\Sigma = M$
    \item $S = 2^M$
    \item $S_0 = \left\{ ms \in S\ |\ \exists c \in C.\ inv_{ms}(c) \land init(c) \right\}$
    \item Para todo $ms \in S$ y $m \in \Sigma$,
    \begin{enumerate}
        \item si $m \notin ms$ entonces $\delta(ms, m) = \emptyset$
        \item si no $\delta(ms, m) = \left\{ ns \in S\ |\ \exists c \in C.\ inv_{ms}(c) \land inv_{ns}(f_m(c)) \right\}$
    \end{enumerate}
\end{enumerate}

Notar que el punto 2 define a $S$ como el conjunto de partes del conjunto finito $M$, con lo cual la EPA caracterizada resulta ser un LTS finito. Además, la función de transición se define como $\delta: S \times \Sigma \rightarrow \mathcal{P}(S)$, cuya imágen es el conjunto de partes de S, por lo tanto, el LTS resultante es no determinístico.