Habiendo realizado pruebas en una escala pequeña para comprender las fortalezas y debilidades del uso de \Corral para el análisis de código seguimos con el estudio de ejemplos concretos. Recurrimos a los casos utilizados en \cite{ZoppiBCGU11} y en \cite{DeCaso2011} junto con algunos ejemplos nuevos que no habían sido evaluados previamente\footnote{Los ejemplos y la implementación de la herramienta \ContractorNET están disponibles para ser descargados en: \url{http://lafhis.dc.uba.ar/contractor/contractor.net-web/}}. El hecho de usar casos de estudio ya validados previamente nos permite tener un punto de comparación para poder responder los interrogantes que nos planteamos. Sin embargo, agregar algunos casos nuevos nos pareció interesante para poder enriquecer las situaciones evaluadas.

\subsubsection{RQ1: ¿La precisión de las abstracciones generadas es adecuada para validar el modelo?}
Quizá una de las preguntas fundamentales que toda implementación del algoritmo de generación de EPAs debe responder es si es capaz de generar EPAs precisas. Para poder contestar esto es necesario primero introducir una noción de precisión de las EPAs. Para esto vamos a recurrir a la medida de precisión utilizada en \cite{ZoppiBCGU11}. Esto quiere decir que para determinar cuán precisa es una EPA vamos a considerar el porcentaje de queries generadas que tuvo un resultado concluyente.

Luego, procedimos a analizar nuestros casos de uso tanto con \Corral como con \CodeContracts obteniendo los resultados que se pueden observar en la Figura \ref{fig:precision-corral-cc}. Lo primero que sale a la luz es que utilizar \Corral permite alcanzar la máxima precisión en la mayoría de las oportunidades. No es casual que en el caso del GenericStackSet$<$T$>$ \Corral pierda precisión. Dicho ejemplo, agregado en este trabajo, caracteriza un stack que no permite agregar elementos duplicados. Para poder llevar esto a cabo la estructura de datos utiliza un ciclo que controla que el elemento a agregar al stack no haya sido agregado previamente. Como era esperado, por los resultados obtenidos en los casos básicos, el uso de ciclos trae complicaciones a \Corral.

\begin{figure}[h]
	\centering
	\includegraphics[scale=.5]{imgs/precision-corral-cc.png}
	\caption{Comparación entre la precisión de \Corral y la de \CodeContracts}\label{fig:precision-corral-cc}
\end{figure}

Por su parte, \CodeContracts logró una exactitud de los resultados razonable en la mayoría de los casos. Naturalmente, los casos que no habían sido contemplados lograron perjudicar la precisión del resultado. Particularmente resalta el caso del ListItr. Este caso es la reescritura de la clase ListItr del JDK de Java, utilizado en \cite{DeCaso2011}. Acá se hace notorio que el no utilizar un model checker impide que se puedan analizar casos de escala real y por ende más complejos.

\subsubsection{RQ2: ¿Se generan las EPAs en un tiempo aceptable?}
Es notable la exactitud con la que \Corral puede resolver la gran mayoría de las consultas. Sin embargo, si el tiempo necesario para obtener tanta precisión crece de forma desproporcionada las ventajas de esta implementación no serían tan significativas. Por esta razón realizamos una comparación entre el tiempo de análisis necesario para conseguir las EPAs para nuestros casos de estudio.

Como se puede apreciar en la Figura \ref{fig:tiempo-corral-cc} los tiempos de ejecución de ambas implementaciones son muy similares en un gran porcentaje de los casos evaluados. Aunque esta performance sin lugar a dudas es un gran acierto para \Corral contra \CodeContracts, nos lleva a preguntarnos por qué es que esto sucede. Para encontrar la respuesta es necesario analizar con más detalle cada ejemplo.

\begin{figure}[h]
	\centering
	\includegraphics[scale=.5]{imgs/tiempo-corral-cc.png}
	\caption{Comparación entre el tiempo de análisis de \Corral y el de \CodeContracts}\label{fig:tiempo-corral-cc}
\end{figure}

%\marginpar{OBTENER CANTIDAD DE QUERIES!}
Los primeros dos casos `Linear' y `VendingMachine' son demasiado sencillos y pequeños como para que se acentúen diferencias en los analizadores. Más aún, en ambos casos los dos analizadores obtuvieron porcentajes de precisión muy altos.

Más interesantes para analizar son las dos representaciones de stack `FiniteStack' y \\`GenericStackSet$<$T$>$'. En estos casos se visualiza una baja considerable en la precisión de la EPA generada por \CodeContracts. 

Si bien estos ejemplos representan un incremento de dificultad, siempre manteniendo una escala chica, el aumento de interacción interprocedural empieza a dificultar la tarea de \CodeContracts. Esto se debe a que inicialmente, el objetivo de \CodeContracts como tecnología fue la demostración formal intraprocedural. Aunque posteriormente fue incorporada la posibilidad de realizar demostraciones utilizando un análisis interprocedural, la interacción entre distintos métodos es una debilidad de la tecnología. Por esta razón, las precisión de \CodeContracts cae considerablemente. Sin embargo, se puede visualizar en el caso de `GenericStackSet$<$T$>$' que el hecho de contar con un análisis más liviano, y la inferencia de invariantes, permite equiparar la penalidad por la precisión que paga \Corral. 

Prestemos atención en este momento a los casos `Door' y `ATM'. Estas clases son interesantes ya que muestran por primera vez ejemplos en donde el tiempo de ejecución de \Corral supera significativamente al de \CodeContracts. En primer lugar, reafirmando lo mencionado anteriormente, al acceder directamente a los campos de la clase, en vez de a través de una propiedad ayuda considerablemente a \CodeContracts. Notar que la precisión del análisis vuelve a índices muy altos. En segundo lugar, las EPAs resultantes de ambos ejemplos tienen un alto nivel de conectividad. La interacción entre los métodos es elevada y por lo tanto la cantidad de queries analizadas empieza a ser un factor determinante en el tiempo de ejecución. Naturalmente, las llamadas al SMT solver tienen como contrapartida un tiempo de ejecución más largo.

Por último se encuentran `LinkedList' y `ListItr', dos implementaciones cercanas a lo que uno se puede encontrar al analizar código. Estas son las clases en las cuales \Corral logra la mayor diferencia con respecto a \CodeContracts, dando pruebas concretas de los beneficios de utilizar \Corral para la construcción de las EPAs. El hecho de lograr una precisión tan baja en el caso de `ListItr' provoca que se sobre-aproxime demasiado la EPA aumentando considerablemente la cantidad de queries a resolver. Aún siendo mucho más ligero para contestarlas, esto es un factor determinante en la construcción de las EPAs.

\subsubsection{RQ2.1: ¿Es beneficioso el uso de paralelismo con los analizadores existentes?}
Los resultados visualizados anteriormente son los mejores obtenidos al utilizar ambas implementaciones. Dado que con el objetivo de reducir el tiempo necesario para construir EPAs, realizamos una implementación parcial de una estrategia paralela, es necesaria una evaluación de los resultados logrados para determinar la eficiencia de la misma. 

Para esto, realizamos una comparación entre los tiempos necesarios para obtener las EPAs usando la versión secuencial y la nueva implementación paralela. Debido a que la inclusión de procesamiento paralelo impacta tanto sobre el algoritmo general de creación de EPAs como también a la ejecución propia del model checker es necesario analizar ambos ejes.

\begin{figure}[H]
	\centering
	\includegraphics[scale=.5]{imgs/corral-single-parallel.png}
	\caption{Comparación entre la versión secuencial y la paralela usando \Corral}\label{fig:corral-single-parallel}
\end{figure}

Al observar los tiempos necesarios para obtener las EPAs al usar \Corral tanto en su versión secuencial como paralela (Figura \ref{fig:corral-single-parallel}) notamos una reducción significativa de los valores. Sin dudas, esto marca un claro beneficio en la incorporación de paralelismo al utilizar \Corral. Sin embargo, dado que \Corral hace uso del paralelismo en los dos ejes mencionados previamente, es necesario evaluar los efectos de los cambios en el algoritmo general. En este sentido, repetimos el experimento utilizando \CodeContracts ya que únicamente hace uso de las modificaciones en el algoritmo general.

\begin{figure}[H]
	\centering
	\includegraphics[scale=.5]{imgs/cc-single-parallel.png}
	\caption{Comparación entre la versión secuencial y la paralela usando \CodeContracts}\label{fig:cc-single-parallel}
\end{figure}

Afortunadamente, las mismas conclusiones aplican para el algoritmo general. En la Figura \ref{fig:cc-single-parallel} se vuelve a repetir los observado al utilizar \Corral. En todos los casos, se redujo considerablemente el tiempo necesario para obtener la EPA. Esto nos permite afirmar que más allá de los costos extras inherentes a la programación paralela, esta nos abre posibilidades de continuar mejorando la eficiencia de este prototipo.

\subsubsection{RQ3: ¿Esta nueva implementación es lo suficientemente robusta como para manejar programas complejos?}
Por último, es interesante discutir acerca de la capacidad de este prototipo de procesar la gran variedad de clases que existe. Como referencia, en la Figura \ref{fig:precision-tiempo} podemos ver una versión compacta y agregada de los resultados previos.

Es indudable el poder de \Corral como model checker. Tanto en los casos básicos descriptos anteriormente como con los ejemplos concretos considerados obtuvo resultados satisfactorios. En particular, para estos últimos pudo conseguir una precisión virtualmente máxima. Aún así, en algunas ocasiones el costo del model checker es mayor al necesario para el análisis. En estos casos, gracias a las características de \CodeContracts, podemos lograr resultados igualmente precisos sin tanto overhead de análisis. Por lo tanto, si bien no podemos decir que \Corral sea una alternativa completamente superadora, consideramos que se complementa muy bien con la implementación previa.

\begin{figure}[H]
	\centering
	\includegraphics[scale=.5]{imgs/precision-tiempo.png}
	\caption{Relación entre la precisión y tiempo de generación de la EPA}\label{fig:precision-tiempo}
\end{figure}

Con todos los avances desarrollados para está nueva versión del prototipo \ContractorNET logramos alcanzar el estudio de casos reales en tiempos razonables para la complejidad inherente a las consultas involucradas. Desde luego, todavía hay muchas posibilidades de lograr mejores resultados, de todas formas, la combinación de herramientas de resolución define un hito en el manejo de programas complejos. 