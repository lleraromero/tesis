La mayoría de los verificadores automáticos de programas plantean la verificación como un problema de decisión que intenta responder a la siguiente pregunta: \emph{¿existe una prueba que demuestre la ausencia de errores?} Encontrar una respuesta es un problema indecidible, más aún, no es ni siquiera recursivamente enumerable, lo que invita a pensar nuevas alternativas para intentar resolver este complejo desafío. 

Por este motivo, el enfoque planteado para desarrollar \Corral fue distinto \cite{LalQadeer2013}. La propuesta fue considerar el problema de verificación como un problema de decisión que responda a la pregunta \emph{¿existe una ejecución que muestre la presencia de un error?} Aunque este problema sigue siendo indecidible,  es sin embargo recursivamente enumerable \cite{LalQadeer2013}.

Atacar la verificación bajo este punto de vista presenta algunas ventajas con respecto a la versión clásica. En primer lugar, permite la formulación de problemas acotados y decidibles.

Además, se asemeja bastante a la idea de \emph{bug-finding} y \emph{debugging} que suelen ser las formas más frecuentemente utilizadas por los desarrolladores para justificar que un programa ``funciona bien'' \footnote{Aunque E.W. Dijkstra no esté muy de acuerdo: ``Program testing may convincingly demonstrate the presence of bugs, but can never demonstrate their absence''.}.

Por último, es importante destacar que la búsqueda de contraejemplos no invalida el uso de demostraciones formales. Por el contrario, estas se transforman en una oportunidad de optimización en las búsquedas, más que ser un fin en sí mismas.

Teniendo en cuenta estos lineamientos, se ideó la arquitectura de \Corral (Figura \ref{fig:Corral-arq}), que permite entender los componentes principales que interactúan dentro del mismo para llevar a cabo el análisis del código.

\begin{figure}[H]
	\centering
	\includegraphics[scale=.4]{imgs/corral-architecture.png}
	\caption{Arquitectura de \Corral \label{fig:Corral-arq}}
\end{figure}

Es importante notar, que el lenguaje de código fuente que \Corral es capaz de analizar es \Boogie \cite{BarnettChangDeLineEtAl2005}. 
Uno de los motivos de esta elección, argumentan sus creadores, es que permite modelar las estructuras de control clásicas, tipos no acotados y operaciones como aritméticas y funciones no interpretadas, que abarcan gran parte de las funcionalidades de lenguajes utilizados en la industria tales como C\# o Java. Además, \Boogie es capaz de generar \emph{verification conditions}, a partir de porciones de código con un \emph{assert}, que pueden ser resueltas con un \emph{SMT solver} como Z3. Dichas verification conditions serán satisfactibles si y sólo si existe una ejecución del código que viole el assert.

\Corral usa un ciclo de \emph{refinamiento de abstracciones guiado por contraejemplos}, conocido como CEGAR loop \cite{ClarkeKurshanVeith2010} por sus siglas en inglés, de dos niveles.\\
El ciclo del primer nivel realiza una abstracción sobre las variables globales del programa. Dado un conjunto de variables que están siendo consideradas en un momento dado, se abstrae el programa utilizando dicho conjunto; el conjunto inicial de variables bajo análisis es vacío. Luego, se pasa esta abstracción al módulo de \emph{Stratified Inlining} para buscar una traza que haga fallar el programa.

En caso de encontrarse una ejecución que lleve a un error puede ser por dos motivos, que surja por no haber considerado alguna variable global, o bien, que efectivamente el programa puede hacer falso un assert. Para poder determinar esto, se chequea esta traza contra el uso de todas las variables globales. Si de todas formas se sigue violando el assert, se concluye que es un bug. En caso contrario, se llama al módulo de \emph{Hierarchical Refinement} que incrementa el conjunto de variables \emph{trackeadas} con el mínimo conjunto de variables necesarias para que el contraejemplo pase a ser espurio.

Es importante notar que ambos módulos, Stratified Inlining y Hierarchical Refinement hacen múltiples llamadas al SMT solver para realizar sus tareas.